import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        RouletteWheel rouletteWheel = new RouletteWheel();

        int userMoney = 1000;
        String userChoice;
        int userNumberBet;
        int userMoneyBet;
        boolean gameOver = false;

        System.out.println("Welcome to Roulette!");
        System.out.println("You start with $" + userMoney);

        while (!gameOver) {
            if (userMoney <= 0) {
                System.out.println("Game over! You've run out of money.");
                break;
            }

            System.out.println("Would you like to bet? (Yes/No)");
            userChoice = scan.next();

            if (userChoice.equalsIgnoreCase("No")) {
                System.out.println("Thanks for playing! You end with $" + userMoney);
                gameOver = true;
                break;
            }

            System.out.println("Which number would you like to bet on (0-36)?");
            userNumberBet = scan.nextInt();

            while (userNumberBet < 0 || userNumberBet > 36) {
                System.out.println("Invalid input, choose a number between 0 and 36.");
                userNumberBet = scan.nextInt();
            }

            System.out.println("How much money do you want to bet?");
            userMoneyBet = scan.nextInt();

            while (userMoneyBet <= 0 || userMoneyBet > userMoney) {
                System.out.println("Invalid input, choose a valid bet amount.");
                userMoneyBet = scan.nextInt();
            }

            rouletteWheel.spin();
            int spinResult = rouletteWheel.getValue();

            System.out.println("The wheel spins, and the result is: " + spinResult);

            if (spinResult == userNumberBet) {
                userMoney += userMoneyBet * spinResult;
                System.out.println("Congratulations! You won $" + (userMoneyBet * spinResult));
            } else {
                userMoney -= userMoneyBet;
                System.out.println("Sorry, you lost $" + userMoneyBet);
            }

            System.out.println("Your remaining balance: $" + userMoney);
        }
    }
}

